import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { RequestService } from "src/app/core/service/request.service"

@Component({
  selector: 'app-view-allrequests',
  templateUrl: './view-allrequests.component.html',
  styleUrls: ['./view-allrequests.component.css']
})
export class ViewALLRequestsComponent implements OnInit {

  @ViewChild("targetDataGrid", { static: false }) dataGrid: DxDataGridComponent | undefined
  requests: any;
  height = 0;
  employees = {};
  numberOfRequests: number = 0;
  requestsInPage = 10;
  numberOfPages = 0;
  arrayOfPages: any = [];

  constructor(private http: HttpClient,
    private requestService: RequestService
  ) { }

  ngOnInit(): void {
    this.height = window.innerHeight - 100;
    this.requestService.getRequests().subscribe(data => {
      this.requests = data
      this.numberOfRequests = (this.requests).length
      this.createOptionsOfPages()
    })
  }
  // create pages for UI
  createOptionsOfPages() {
    this.numberOfPages = Math.ceil(this.numberOfRequests / this.requestsInPage)
    for (let i: number = 1; i <= this.numberOfPages; i++) {
      this.arrayOfPages.push(i)
    }
  }
  // pagintation
  pagintation(e: any) {
    console.log(e.target.value);
    this.dataGrid?.instance.pageIndex(e.target.value - 1);
  }
  onSearchChanged(e: any) {
    console.log(e)
  }
}
