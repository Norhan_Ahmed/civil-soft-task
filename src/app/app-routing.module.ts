import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// -----------------components------------- //
import { CreateRquestComponent } from 'src/app/create-rquest/create-rquest.component'
import { ViewALLRequestsComponent } from 'src/app/view-allrequests/view-allrequests.component'
import { NotFoundComponent } from 'src/app/not-found/not-found.component'

const routes: Routes = [
  {
    path: 'create-request',
    component: CreateRquestComponent,
    data: {
      title: 'CreateRequest'
    }
  },
  {
    path: "view-requests",
    component: ViewALLRequestsComponent,
    data: {
      title: 'CreateRequest'
    }
  },
  {
    path: '',
    redirectTo: "create-request",
    pathMatch: "full"
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {
      title: 'CreateRequest'
    }
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
