import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { MatIconModule } from '@angular/material/icon';
import {
  DxSelectBoxModule,
  DxTextBoxModule,
  DxDateBoxModule,
  DxDataGridModule
} from 'devextreme-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/////---------------components------------/////
import { AppComponent } from './app.component';
/////----------------service-------------/////
import { RequestService } from 'src/app/core/service/request.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateRquestComponent } from './create-rquest/create-rquest.component';
import { ViewALLRequestsComponent } from './view-allrequests/view-allrequests.component';
import { NotFoundComponent } from './not-found/not-found.component'
@NgModule({
  declarations: [
    AppComponent,
    CreateRquestComponent,
    ViewALLRequestsComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxDateBoxModule,
    DxDataGridModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [RequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
