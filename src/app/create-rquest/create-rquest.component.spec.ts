import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRquestComponent } from './create-rquest.component';

describe('CreateRquestComponent', () => {
  let component: CreateRquestComponent;
  let fixture: ComponentFixture<CreateRquestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateRquestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRquestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
