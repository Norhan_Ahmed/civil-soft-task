export interface Request {
    code: number;
    reqNo: number;
    image: string;
    employeeName: string;
    jobTitle: string;
    salaryProfile: string;
    expectedLeaveStartDate: string;
    expectedLeaveRejoinDate: string;
    actualLeaving: string;
    leaveType: string;
}