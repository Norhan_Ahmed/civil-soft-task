import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class EmployeesService {

    apiUrl = "http://localhost:3000/employees"

    constructor(private _http: HttpClient) { }

    getEmployees() {
        return this._http.get(this.apiUrl)
    }
}
