import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class RequestService {

    apiUrl = "http://localhost:3000/requests"

    constructor(private _http: HttpClient) { }

    getRequests() {
        return this._http.get(this.apiUrl)
    }

    creatRequest(request: object) {
        console.log("creatRequest")
        return this._http.post(this.apiUrl, request)
    }

}
