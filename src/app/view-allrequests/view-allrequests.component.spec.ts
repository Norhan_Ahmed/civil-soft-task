import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewALLRequestsComponent } from './view-allrequests.component';

describe('ViewALLRequestsComponent', () => {
  let component: ViewALLRequestsComponent;
  let fixture: ComponentFixture<ViewALLRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewALLRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewALLRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
