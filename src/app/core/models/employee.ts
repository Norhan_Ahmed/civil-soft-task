export interface Employee {
    name: string;
    code: number;
    jobTitle: string;
    salaryProfile: string;
    joiningData: string;
    location: string;
    ImageSrc: string;
}