import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// ----------------------models------------------- //
import { Employee } from 'src/app/core/models/employee'
import { Request } from 'src/app/core/models/request'
// ----------------------service------------------ //
import { EmployeesService } from 'src/app/core/service/employees.service'
import { RequestService } from 'src/app/core/service/request.service'


@Component({
  selector: 'app-create-rquest',
  templateUrl: './create-rquest.component.html',
  styleUrls: ['./create-rquest.component.css']
})
export class CreateRquestComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  leaveTypeList: FormArray = new FormArray([]);
  fileUploaded: boolean = false;
  dateNow: Date = new Date();
  formattedDate = this.dateNow.getDate() + '/' + (this.dateNow.getMonth() + 1) + '/' + this.dateNow.getFullYear();
  leaveType: Array<string> = ["Sick Leave", "Annual leave with payroll", "Annual leave during Reserve"]
  replacement: Array<string> = ["Ahmed", "Mohamed", "Ali"]
  guarantors: Array<string> = ["Ahmed", "Mohamed", "Ali"]
  employees: any;
  employee = {} as Employee;
  request = {} as Request;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private employeesService: EmployeesService,
    private requestService: RequestService,
    private router: Router) { }

  ngOnInit() {
    // initialize 
    this.employee.name = ""
    this.getEmployees()
    this.formVariables()
  }

  //get all employees data
  getEmployees() {
    this.employeesService.getEmployees().subscribe(data => this.employees = data)
  }

  // initialize form variables
  formVariables() {
    this.form = this.fb.group({
      leaveToAvail: ["Local", Validators.required],
      salaryAdvance: ["no", Validators.required],
      leavingDate: [this.formattedDate, Validators.required],
      rejoiningDate: [this.formattedDate, Validators.required],
      replacement: [null, Validators.required],
      guarantors: [null, Validators.required],
      address: [null, Validators.required],
      contactNo: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      remarks: [null, Validators.required],
      uploadFile: [null, Validators.required],

      leaveTypes: this.fb.array([this.createLeaveType()])
    });

    // set leaveTypeList to this field
    this.leaveTypeList = this.form.get('leaveTypes') as FormArray;
  }
  // contact formgroup
  createLeaveType(): FormGroup {
    return this.fb.group({
      type: [null, Validators.required],
      numofdays: [null, Validators.required]
    });
  }
  // returns all form groups under leaveType
  get leaveTypeFormGroup() {
    return this.form.get('leaveTypes') as FormArray;
  }
  // add a section for leave type 
  addLeaveType() {
    this.leaveTypeList.push(this.createLeaveType());
  }
  // choose leave type 
  leaveTypeChange(e: any, index: number) {
    let value = e.value;
    this.getLeaveTypeFormGroup(index).controls['type'].setValue(value)
    this.getLeaveTypeFormGroup(index).controls['type'].updateValueAndValidity();
  }
  // get the formgroup under leaveTypeList form array
  getLeaveTypeFormGroup(index: number): FormGroup {
    const formGroup = this.leaveTypeList.controls[index] as FormGroup;
    return formGroup;
  }
  // get value of searching employee
  onSearchChanged(e: any) {
    this.employee = e.value
    this.request.code = this.employee.code
    this.request.reqNo = this.employee.code
    this.request.employeeName = this.employee.name
    this.request.jobTitle = this.employee.jobTitle
    this.request.salaryProfile = this.employee.salaryProfile
    this.request.image = this.employee.ImageSrc
  }
  // get value of leaving date
  onLeavingDateChanged(e: any) {
    var newDate = e.value
    var formattingDate = (newDate.getDate() + '/' + (newDate.getMonth() + 1) + '/' + newDate.getFullYear())
    this.form.value.leavingDate = formattingDate
  }
  // get value of joining date
  onRejoiningDateChanged(e: any) {
    var newDate = e.value
    var formattingDate = (newDate.getDate() + '/' + (newDate.getMonth() + 1) + '/' + newDate.getFullYear())
    this.form.value.rejoiningDate = formattingDate
  }
  // get value of joining date
  onGuarantorsChange(e: any) {
    this.form.controls['guarantors'].setValue(e.value)
    this.form.controls['guarantors'].updateValueAndValidity();
  }
  // get value of joining date
  onReplacementChange(e: any) {
    this.form.controls['replacement'].setValue(e.value)
    this.form.controls['replacement'].updateValueAndValidity();
  }
  // upload file 
  getNameOfFile(event: any) {
    this.fileUploaded = true
    const file = event.target.files[0];
    this.form.controls['uploadFile'].setValue(file)
    this.form.controls['uploadFile'].updateValueAndValidity();
  }
  // form is submitted
  saveRequest() {
    let numberOfLeaveType = (this.form.value.leaveTypes).length;
    this.request.expectedLeaveStartDate = this.form.value.leavingDate
    this.request.expectedLeaveRejoinDate = this.form.value.rejoiningDate
    for (let i = 0; i < numberOfLeaveType; i++) {
      this.request.leaveType = this.form.value.leaveTypes[i].type
      // send data
      this.requestService.creatRequest(this.request).subscribe(data => console.log(data))
    }
    //if data send successfully ( data.state == 200)
    this.router.navigate(['/view-requests'])
  }
}
